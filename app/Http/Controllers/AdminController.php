<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\AdminCreateRequest;
use App\Http\Requests\AdminUpdateRequest;

class AdminController extends Controller
{
    //


    public function __construct(){
        $this->middleware('admin');
    }

    public function index(){
        return view('admin/index');
    }

     //listado de docentes
     public function listado_docentes($filtro='id',$orden='asc'){
        //Solo lista los docentes que son tipo de usuario 2 en la base de datos
        $listado=User::where('tipo_usuario','=',2)->orderBy($filtro,$orden)->paginate(10000); 
        if($orden=='asc'){ $orden='desc'; }else{ $orden='asc';  }
       return view('admin.listado')->with('listado',$listado)->with('orden',$orden);
    }


    //Editar docente
    public function editar_docente(Request $request){
        $id_docente=$request->input('id_docente');
        $docente=User::find($id_docente);
        $docente->name=$request->input('name');
        $docente->email=$request->input('email');
       if($docente->save())
        {
         $request->session()->flash('alert-info', 'Actualización exitosa!!');
         return   redirect()->to('/listado_docentes');
        }
        else
        {
         return redirect()->to('/listado_docentes');
        }


    }



     //Crear docente
     public function crear_docente(AdminCreateRequest $request){
       
        $newUser                  = new User;
        $newUser->name            = $request->input('name');
        $newUser->email           = $request->input('email');
        $newUser->google_id       = null;
        $newUser->avatar          = null;
        $newUser->tipo_usuario    = 2;
        if($newUser->save()){
            $request->session()->flash('alert-success', 'Docente creado de manera exitosa!!');
            return redirect()->to('/listado_docentes');
        }else{
            $request->session()->flash('alert-danger', 'No se pudo crear docente!!');
            return redirect()->to('/listado_docentes');
        }
       
     
    }


}
