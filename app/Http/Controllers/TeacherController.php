<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function __construct(){
        $this->middleware('docente');
    }

    public function index(){
        return view('Teacher/index');
    }
}
