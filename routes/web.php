<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('homew');
});
Route::get('/hom', function () {
        return view('hom');
    });
    

//Rutas para administrador
Route::get('/admin', 'AdminController@index');
Route::get('listado_docentes/{filtro?}/{orden?}', 'AdminController@listado_docentes')->name('listado');
Route::get('form_editar_docente/{id}', 'AdminController@form_editar_docente');
Route::post('editar_docente', 'AdminController@editar_docente');
Route::post('crear_docente', 'AdminController@crear_docente');
   
//Rutas para estudiante   
Route::get('/student', 'StudentController@index');
//Rutas para profesor
Route::get('/teacher','TeacherController@index');




//Validación para inicio de sesión con google
Route::get('homew/{provider?}', 'Auth\LoginController@getSocialAuth');
Route::get('homew/{provider?}/logout', 'Auth\LoginController@logout');
Route::get('/homew/{provider?}/callback','Auth\LoginController@handleProviderCallback');

        