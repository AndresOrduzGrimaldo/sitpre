<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SITPRE - Manejo de terceros previos</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <!-- Google fonts - Poppins-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,600">
    <!-- Lightbox-->
    <link rel="stylesheet" href="vendor/lightbox2/css/lightbox.css">
    <!-- Custom font icons-->
    <link rel="stylesheet" href="css/fontastic.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon1.png">


    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg fixed-top" >
        <div class="container"><a href="./" class="navbar-brand"><img src="img/logo3.svg" alt="" class="img-fluid"></a>
          <button type="button" data-toggle="collapse" data-target="#navbarSupportedContent" 
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right" style="color:white">Menu<i class="fa fa-bars ml-2"></i></button>
          <div id="navbarSupportedContent" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                  <!-- Link-->
                  <li class="nav-item" > <a href="./" class="nav-link"  >HOME</a></li>

                  <li class="nav-item"> <a href="https://divisist2.ufps.edu.co" target="_blank" class="nav-link">DIVISIST</a></li>
                  <li class="nav-item"> <a href="https://ww2.ufps.edu.co" target="_blank" class="nav-link" >INSTITUCIONAL</a></li>
                  <li class="nav-item"> <a href="https://ingsistemas.cloud.ufps.edu.co" target="_blank" class="nav-link" >PROGRAMA ACADEMICO</a></li>
                  
                  <!-- Link-->
                                  
            </ul>
            @if (Auth::check()&&Auth::user()->tipo_usuario==1)
            <a href="/admin" data-toggle="" data-target="" class="btn btn-danger btn-sm " > 
            <i class="fa fa-user"></i> admin </a>
            @elseif(Auth::check()&&Auth::user()->tipo_usuario==2)    
             <a href="/teacher" data-toggle="" data-target="" class="btn btn-danger btn-sm " > 
            <i class="fa fa-user"></i> docente </a>  
            @elseif(Auth::check()&&Auth::user()->tipo_usuario==null)    
             <a href="/student" data-toggle="" data-target="" class="btn btn-danger btn-sm " > 
            <i class="fa fa-user"></i> estudiante </a>           
            @else
             <button href="#" data-toggle="modal" data-target="#login" class="btn btn-danger btn-sm " > 
             <i class="fa fa-user"></i> Iniciar Sesión </button>
             @endif
          </div>
        </div>
      </nav>
    </header>
    <!-- Login Modal-->
    <div id="login" tabindex="-1" role="dialog" aria-hidden="true" class="modal fade bd-example-modal-lg">
      <div role="document" class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
          <div class="modal-header border-bottom-0">
            <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true" >×</span></button>
          </div>
          <div class="modal-body p-4 p-lg-5">
            <div class="form-group" align="Center">
              <h2>Bienvenido a <strong>SITPRE</strong></h2>
              <img src="img/logo2.svg" width="300" height="200" align="Center">
              <h4>SISTEMA PARA EL MANEJO DE TERCEROS PREVIOS</h4>
                
              <a href="{{ url('homew/google') }}" class="btn btn-danger"><i class="fa fa-google-plus"></i> Iniciar Sesión con Google</a>
               </div>
         </div>
        </div>
      </div>
    </div>
   <!----------------------------------------------------------------->
   
    <!-- Divider Section-->
    <section>
      <div class="container">
        <div class="row">
        
            <div class="col-lg-8 mb-5 mb-lg-0" >
            
           <img src="./img/logo_sistemas.png" width="90%"  alt="Logo Ingeniera de Sistemas" >
            </div>
            <div class="col-lg-4">
           <img src="./img/logo_ufps.png"  alt="Logo  UFPS">
            </div>
          </div>
      </div>
    </section>
  

  <section class="extra">
      <div class="container">
        <div class="text-center">
          <h2>Ventajas</h2>
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <p class="lead text-muted mt-2">Sistema de información para el manejo de terceros previos.</p>
            </div>
          </div>
        </div>
        <div class="integrations mt-5">
          <div class="row">
       
            <div class="col-lg-4">
              <div class="box text-center">
                <div class="icon d-flex align-items-end"><img src="img/pen.svg" alt="..." class="img-fluid"></div>
                <h3 class="h4">Manejo Confidencial de notas</h3>
                <p class="text-small font-weight-light">Lorem Ipsum has been the industry's standard dummy text ever.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="box text-center">
                <div class="icon d-flex align-items-end"><img src="img/chat.svg" alt="..." class="img-fluid"></div>
                <h3 class="h4">Seguridad de información</h3>
                <p class="text-small font-weight-light">Lorem Ipsum has been the industry's standard dummy text ever.</p>
              </div>
            </div>
            <div class="col-lg-4">
              <div class="box text-center">
                <div class="icon d-flex align-items-end"><img src="img/idea.svg" alt="..." class="img-fluid"></div>
                <h3 class="h4">Consula de notas</h3>
                <p class="text-small font-weight-light">Lorem Ipsum has been the industry's standard dummy text ever.</p>
              </div>
            </div>
          </div>
    </section>

  <section class="extra">
  <h1 align="center">Estructura de la plantilla</h1>
  <div class="video-responsive">
  
<iframe  src="https://www.youtube.com/embed/sOnqjkJTMaA" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
</div>

  </section>
    
    
    <footer class="main-footer" >
      <div class="container">
        <div class="row">
          <div class="col-lg-3 mb-5 mb-lg-0">
            <div class="footer-logo"><img src="img/logo3.svg" alt="..." class="img-fluid"></div>
          </div>
          <div class="col-lg-2 mb-5 mb-lg-0">
            <h5 class="footer-heading">Páginas</h5>
            
            
           <div class="col-ms-2">
               <a href="index.html" class="footer-link">Home</a>
          </div>
          <div class="col-ms-2">
             <a href="faq.html" class="footer-link">FAQ</a>
          </div>
          <div class="col-ms-2">
           <a href="contact.html" class="footer-link">Contact</a>
          </div>
         
          </div>
           
          <div class="col-lg-3 mb-5 mb-lg-0">
            <div class="footer-logo"><img src="./img/logo_ufps.png" alt="..." class="img-fluid"></div>
        </div>
        </div>
      </div>
      <div class="copyrights">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 text-center text-lg-left">
              <p class="copyrights-text mb-3 mb-lg-0">&copy; Todos los derechos reservados. UFPS.</p>
              
            </div>
            <div class="col-lg-6 text-center text-lg-right">
              <ul class="list-inline social mb-0">
                <li class="list-inline-item"><a href="#" class="social-link"><i class="fa fa-facebook"></i></a><a href="#" class="social-link"><i class="fa fa-twitter"></i></a><a href="#" class="social-link"><i class="fa fa-youtube-play"></i></a><a href="#" class="social-link"><i class="fa fa-vimeo"></i></a><a href="#" class="social-link"><i class="fa fa-pinterest"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/lightbox2/js/lightbox.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>